import React, { Component } from 'react';
import { TabNavigator,StackNavigator,DrawerNavigator } from 'react-navigation';
import {MainUI} from './ui/MainUI';
import SearchUI from './ui/SearchUI';
import OrderUI from './ui/OrderUI';
import CityUI from './ui/CityUI';
const StackNav = StackNavigator({
  MainUI:{
    screen:MainUI,
  },
  SearchUI:{
    screen:SearchUI,
    navigationOptions:{
      header:null,
    }
  },
  OrderUI:{
    screen:OrderUI,
    navigationOptions:{
      header:null,
    }
  },
  CityUI:{
    screen:CityUI,
  }
},{
  initialRouteName: /*'SearchUI,CityUI'*/'MainUI', // 默认显示界面
});
export default class App extends Component<{}> {
  render() {
      // return(<StackNav />);
      return(<StackNav/>)
  }
}
