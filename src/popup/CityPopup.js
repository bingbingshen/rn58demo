import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableHighlight,
    Animated,
    Easing,
    Dimensions,
    Platform,
    TouchableOpacity,
    FlatList
} from 'react-native';

const {width, height} = Dimensions.get('window');
const [left, top] = [0, 0];
const bottom_interval = 100;
const regions =[{id:0,name:'全北京',son:[]},
              {id:1,name:'朝阳',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:2,name:'海淀',son:[{id:0,name:'全海淀'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:3,name:'东城',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:4,name:'西城',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:5,name:'崇文',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:6,name:'玄武',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:7,name:'丰台',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:8,name:'通州',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:9,name:'石景山',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:10,name:'房山',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:11,name:'昌平',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:12,name:'大兴',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]},
              {id:13,name:'顺义',son:[{id:0,name:'全朝阳'},{id:1,name:'CBD'},{id:2,name:'安慧桥'},{id:3,name:'安贞'}]}]
class RegionListItem extends React.PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.id);
  };

  render() {
    return (
      <TouchableOpacity
        {...this.props}
        onPress={this._onPress}
      >
      {this._renderItem()}
      </TouchableOpacity>
    )
  }

  _renderItem(){
    console.log(this.props.selectedid+'_'+this.props.id)
    if(this.props.selectedid == this.props.id){
        return(
          <View style={[styles.region_item,{backgroundColor:'#f8f9fb'}]}>
            <View style={styles.slectline}></View>
            <Text style={styles.region_item_font}>{this.props.item.name}</Text>
          </View>
          )
    }else{
        return (
          <View style={styles.region_item}>
            <Text style={styles.region_item_font}>{this.props.item.name}</Text>
          </View>
        );
    }
  }
}
class RegionSonListItem extends React.PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.id);
  };
  render() {
    return (
      <TouchableOpacity
        {...this.props}
        onPress={this._onPress}
      >
      <View style={styles.region_item}>
        <Text style={styles.region_item_font}>{this.props.item.name}</Text>
      </View>
      </TouchableOpacity>
    )
  }
}
export default class CityPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            offset: new Animated.Value(0),
            opacity: new Animated.Value(0),
            title: "",
            choose0: "",
            choose1: "",
            hide: true,
            tipTextColor: '#333333',
            aHeight: 0,
            top:0,
            selected: (new Map(): Map<string, boolean>),
            regions:regions,
            selectedid:0,
            son_selected: (new Map(): Map<string, boolean>),
            region_sons:regions[0].son,
        };
        this.entityList = [];//数据源
        this.callback = function () {
        };//回调方法
    }

    render() {
        if (this.state.hide) {
            return (<View />)
        } else {
            return (
                <View style={{
                  position: "absolute",
                  width: width,
                  height: height,
                  left: left,
                  top: this.state.top,
                  backgroundColor:'rgba(0,0,0,0.5)',
                }}>
                    <TouchableHighlight
                        onPress={this.cancel.bind(this)}
                    >
                      <Animated.View style={styles.mask}>
                      </Animated.View>
                    </TouchableHighlight>
                    <Animated.View style={[{
                        width: width,
                        height: this.state.aHeight,
                        left: 0,
                        alignItems: "center",
                        justifyContent: "space-between",
                        backgroundColor:'#ffffff',
                    }, {
                        transform: [{
                            translateY: this.state.offset.interpolate({
                                inputRange: [0,1],
                                outputRange: [-height,0]
                            }),
                        }]
                    }]}>


                        <View style={styles.content}>
                        <FlatList
                          style={styles.region_list}
                          data={this.state.regions}
                          extraData={this.state.selected}
                          keyExtractor={this._keyExtractor}
                          renderItem={this._renderItem}
                          ItemSeparatorComponent={ this._renderItemSeparatorComponent }
                          />
                          <FlatList
                            style={styles.region_son_list}
                            data={this.state.region_sons}
                            extraData={this.state.son_selected}
                            keyExtractor={this._keyExtractor}
                            renderItem={this._renderSonItem}
                            ItemSeparatorComponent={ this._renderItemSeparatorComponent }
                            />
                        </View>

                    </Animated.View>
                </View>
            );
        }
    }


    componentDidMount() {
    }

    componentWillUnmount() {
        // 如果存在this.timer，则使用clearTimeout清空。
        // 如果你使用多个timer，那么用多个变量，或者用个数组来保存引用，然后逐个clear
        this.timer && clearTimeout(this.timer);
        this.chooseTimer && clearTimeout(this.chooseTimer);
    }

    //显示动画
    in() {
        Animated.parallel([
            Animated.timing(
                this.state.opacity,
                {
                    easing: Easing.linear,//一个用于定义曲线的渐变函数
                    duration: 200,//动画持续的时间（单位是毫秒），默认为200。
                    toValue: 0.8,//动画的最终值
                }
            ),
            Animated.timing(
                this.state.offset,
                {
                    easing: Easing.linear,
                    duration: 200,
                    toValue: 1,
                }
            )
        ]).start();
  }

    //隐藏动画
    out() {
        Animated.parallel([
            Animated.timing(
                this.state.opacity,
                {
                    easing: Easing.linear,
                    duration: 200,
                    toValue: 0,
                }
            ),
            Animated.timing(
                this.state.offset,
                {
                    easing: Easing.linear,
                    duration: 200,
                    toValue: 0,
                }
            )
        ]).start((finished) =>{
           this.setState({hide: true});
           this.callback(true)
        });
    }

    //取消
    cancel(event) {
        if (!this.state.hide) {
            this.out();
        }
    }

    //选择
    choose(i) {
        if (!this.state.hide) {
            this.out();
            this.chooseTimer = setTimeout(()=>{
                this.callback(i);
            }, 200);
        }
    }

  /**
  * 弹出控件
  * callback：回调方法
  */
  show(top:number,callback: Object) {
      this.callback = callback;
      this.setState({top:top})
      if (this.state.hide) {
          this.setState({hide: false,aHeight: height-top-bottom_interval}, this.in);
      }
  }

  _keyExtractor = (item, index) => item.id;

  _onPressItem = (id: string) => {
    this.setState({
      selectedid:id,
    })
    var region_sons = [];
    for(var i = 0 ; i < regions.length;i++){
      if(regions[i].id == id){
        region_sons = regions[i].son;
        break;
      }
    }
    if(region_sons.length == 0){
      this.cancel(this);
      return;
    }
    this.setState({
      region_sons:region_sons,
    })
    // updater functions are preferred for transactional updates
    this.setState((state) => {
      // copy the map rather than modifying state.
      const selected = new Map(state.selected);
      selected.set(id, !selected.get(id)); // toggle
      return {selected};
    });
  };
  _onSonPressItem = (id: string) => {
    this.cancel(this);
    // // updater functions are preferred for transactional updates
    // this.setState((state) => {
    //   // copy the map rather than modifying state.
    //   const selected = new Map(state.selected);
    //   selected.set(id, !selected.get(id)); // toggle
    //   return {selected};
    // });
  };
  _renderItemSeparatorComponent = ({highlighted}) => (
       <View style={{ height:1, backgroundColor:'#e0e0e0' }}></View>
   );
  _renderItem = ({item}) => (
    <RegionListItem
      id={item.id}
      onPressItem={this._onPressItem}
      selected={!!this.state.selected.get(item.id)}
      item={item}
      selectedid={this.state.selectedid}
    />
  );
  _renderSonItem = ({item}) => (
    <RegionSonListItem
      id={item.id}
      onPressItem={this._onSonPressItem}
      selected={!!this.state.selected.get(item.id)}
      item={item}
      selectedid={this.state.selectedid}
    />
  );
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        width: width,
        height: height,
        left: left,
        // top: top,
        backgroundColor: "#000000",
    },
    mask: {
        justifyContent: "center",
        backgroundColor: "#000000",
        opacity: 0.3,
        position: "absolute",
        width: width,
        height: height,
        left: left,
        top: top,
    },
    // 提示标题
    tipTitleView: {
        height: 56,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        marginLeft: 10,
        marginRight: 10
    },
    // 提示文字
    tipTitleText: {
        color: "#999999",
        fontSize: 14,
    },
    // 分割线
    tipContentView: {
        width: width,
        height: 56,
        backgroundColor:'#fff',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    item:{
        width: width,
        height: 56,
        backgroundColor:'#fff',
        justifyContent: 'center',
        borderRadius: 5,
    },
    button: {
        height: 57,
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        justifyContent: 'center',
        borderRadius: 5,
    },
    // 取消按钮
    buttonText: {
        fontSize: 17,
        color: "#0084ff",
        textAlign: "center",
    },
    content: {
        backgroundColor: '#fff',
        flexDirection:'row',
        borderTopColor:'#dadada',
        borderTopWidth:1,
    },
    region_list:{
      // width:50,
      flex:1,
      backgroundColor:'#ffffff',
      borderRightColor:'#dadada',
      borderRightWidth:1,
    },
    region_item:{
      paddingTop:10,
      paddingBottom:10,
      flexDirection:'row',
    },
    region_item_font:{
      fontSize:20,
      color:'#000000',
      paddingLeft:14,
    },
    slectline:{
      width:2,
      height:20,
      backgroundColor:'#ff6629',
    },
    region_son_list:{
      flex:1,
      backgroundColor:'#f8f9fb',
    }
});
