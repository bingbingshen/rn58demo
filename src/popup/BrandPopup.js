import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    TouchableHighlight,
    Animated,
    Easing,
    Dimensions,
    Platform,
    TouchableOpacity,
    FlatList,
    SectionList
} from 'react-native';
import BrandSectionList from '../custom/BrandSectionList';
const {width, height} = Dimensions.get('window');
const [left, top] = [0, 0];
const bottom_interval = 100;

const ITEM_HEIGHT = 50; //item的高度
const HEADER_HEIGHT = 24;  //分组头部的高度
const SEPARATOR_HEIGHT = 1;  //分割线的高度
const brands = [
    {showtitle:'全部',title:'全',key:'#',brands:[{brand:'不限'}]},
    {showtitle:'A',title:'A',key:'A',brands:[{brand:'奥迪'},{brand:'ALPINA'}]},
    {showtitle:'B',title:'B',brands:[{brand:'本田'},{brand:'别克'},{brand:'宝马'},{brand:'比亚迪'}]},
    {showtitle:'C',title:'C',brands:[{brand:'长安'},{brand:'长城'}]},
    {showtitle:'D',title:'D',brands:[{brand:'大众'},{brand:'东南'},{brand:'东风'},{brand:'大宇'}]},
    {showtitle:'F',title:'F',brands:[{brand:'丰田'},{brand:'福特'}]},
    {showtitle:'G',title:'G',brands:[{brand:'广汽'},{brand:'观致'}]},
    {showtitle:'H',title:'H',brands:[{brand:'哈飞'},{brand:'海马'}]},
    {showtitle:'J',title:'J',brands:[{brand:'吉利'},{brand:'捷豹'}]},
    {showtitle:'K',title:'K',brands:[{brand:'凯迪拉克'}]},
]
export default class BrandPopup extends Component {
    constructor(props) {
        super(props);
        let jsonData = brands;
        //每组的开头在列表中的位置
        let totalSize = 0;
        //SectionList的数据源
        let cityInfos = [];
        //分组头的数据源
        let citySection = [];
        //分组头在列表中的位置
        let citySectionSize = [];
        console.log(jsonData.length)
        for (let i = 0; i < jsonData.length; i++) {
            citySectionSize[i] = totalSize;
            //给右侧的滚动条进行使用的
            citySection[i] = jsonData[i].title;
            let section = {}
            section.key = jsonData[i].title;
            section.data = jsonData[i].brands;
            for (let j = 0; j < section.data.length; j++) {
                section.data[j].key = j
            }
            cityInfos[i] = section;
            //每一项的header的index
            totalSize += section.data.length + 1
        }
        citySection.unshift('热');
        // console.log('cityInfos:'+JSON.stringify(cityInfos));
        // console.log('citySection:'+citySection);
        // console.log('citySectionSize:'+citySectionSize)
        this.state = {
            offset: new Animated.Value(0),
            opacity: new Animated.Value(0),
            title: "",
            choose0: "",
            choose1: "",
            hide: true,
            tipTextColor: '#333333',
            aHeight: 0,
            top:0,
            selected: (new Map(): Map<string, boolean>),
            data: cityInfos,
            sections: citySection,
            sectionSize: citySectionSize,
              // data: [],
              // sections: [],
              // sectionSize: [],
        };
        this.entityList = [];//数据源
        this.callback = function () {
        };//回调方法
        // this.getCityInfos();
    }
    componentDidMount(){

    }
     async getCityInfos() {
       let data = await require('../assets/city.json');
       let jsonData = data.data
        //每组的开头在列表中的位置
        let totalSize = 0;
        //SectionList的数据源
        let cityInfos = [];
        //分组头的数据源
        let citySection = [];
        //分组头在列表中的位置
        let citySectionSize = [];
        console.log(jsonData.length)
        for (let i = 0; i < jsonData.length; i++) {
            citySectionSize[i] = totalSize;
            //给右侧的滚动条进行使用的
            citySection[i] = jsonData[i].title;
            let section = {}
            section.key = jsonData[i].title;
            section.data = jsonData[i].city;
            for (let j = 0; j < section.data.length; j++) {
                section.data[j].key = j
            }
            cityInfos[i] = section;
            //每一项的header的index
            totalSize += section.data.length + 1
        }
        // console.log('cityInfos:'+JSON.stringify(cityInfos));
        // console.log('citySection:'+citySection);
        // console.log('citySectionSize:'+citySectionSize)
        this.setState({data: cityInfos, sections: citySection, sectionSize: citySectionSize})
    }
    render() {
        if (this.state.hide) {
            return (<View />)
        } else {
            return (
                <View style={{
                  position: "absolute",
                  width: width,
                  height: height,
                  left: left,
                  top: this.state.top,
                  backgroundColor:'rgba(0,0,0,0.5)',
                }}>
                    <TouchableHighlight
                        onPress={this.cancel.bind(this)}
                    >
                      <Animated.View style={styles.mask}>
                      </Animated.View>
                    </TouchableHighlight>
                    <Animated.View style={[{
                        width: width,
                        height: this.state.aHeight,
                        left: 0,
                        alignItems: "center",
                        justifyContent: "space-between",
                        backgroundColor:'#ffffff',
                    }, {
                        transform: [{
                            translateY: this.state.offset.interpolate({
                                inputRange: [0,1],
                                outputRange: [-height,0]
                            }),
                        }]
                    }]}>


                        <View style={styles.content}>
                        <SectionList
                            ref='list'
                            enableEmptySections
                            renderItem={this._renderItem}
                            renderSectionHeader={this._renderSectionHeader}
                            sections={this.state.data}
                            getItemLayout={this._getItemLayout}
                            ItemSeparatorComponent={ this._renderItemSeparatorComponent }
                            ListHeaderComponent={this._renderHeader()}/>

                        <BrandSectionList
                            sections={ this.state.sections}
                            onSectionSelect={this._onSectionselect}/>
                        </View>

                    </Animated.View>
                </View>
            );
        }
    }


    componentDidMount() {
    }

    componentWillUnmount() {
        // 如果存在this.timer，则使用clearTimeout清空。
        // 如果你使用多个timer，那么用多个变量，或者用个数组来保存引用，然后逐个clear
        this.timer && clearTimeout(this.timer);
        this.chooseTimer && clearTimeout(this.chooseTimer);
    }

    //显示动画
    in() {
        Animated.parallel([
            Animated.timing(
                this.state.opacity,
                {
                    easing: Easing.linear,//一个用于定义曲线的渐变函数
                    duration: 200,//动画持续的时间（单位是毫秒），默认为200。
                    toValue: 0.8,//动画的最终值
                }
            ),
            Animated.timing(
                this.state.offset,
                {
                    easing: Easing.linear,
                    duration: 200,
                    toValue: 1,
                }
            )
        ]).start();
  }

    //隐藏动画
    out() {
        Animated.parallel([
            Animated.timing(
                this.state.opacity,
                {
                    easing: Easing.linear,
                    duration: 200,
                    toValue: 0,
                }
            ),
            Animated.timing(
                this.state.offset,
                {
                    easing: Easing.linear,
                    duration: 200,
                    toValue: 0,
                }
            )
        ]).start((finished) =>{
           this.setState({hide: true});
           this.callback(true)
        });
    }

    //取消
    cancel(event) {
        if (!this.state.hide) {
            this.out();
        }
    }

  /**
  * 弹出控件
  * callback：回调方法
  */
  show(top:number,callback: Object) {
    console.log('top:'+top)
      this.callback = callback;
      this.setState({top:top})
      if (this.state.hide) {
          this.setState({hide: false,aHeight: height-top-bottom_interval}, this.in);
      }
  }

  _keyExtractor = (item, index) => item.id;

  _onPressItem = (id: string) => {
    this.cancel(this);
    // updater functions are preferred for transactional updates
    this.setState((state) => {
      // copy the map rather than modifying state.
      const selected = new Map(state.selected);
      selected.set(id, !selected.get(id)); // toggle
      return {selected};
    });
  };
  _renderItemSeparatorComponent = ({highlighted}) => (
       <View style={{ height:1, backgroundColor:'#e0e0e0' }}></View>
   );
   //这边返回的是A,0这样的数据
  _onSectionselect = (section, index) => {
    // console.log(section+'_'+index);
    // console.log(this.state.sectionSize[index]);
      //跳转到某一项
      // this.refs.list.scrollToIndex({animated: true, index: this.state.sectionSize[index]})
      let sectionIndex = 0;
      if(index > 0){
        sectionIndex = index-1;
      }
      console.log('index:'+index+'_sectionIndex:'+sectionIndex);
      this.refs.list.scrollToLocation({ animated:false,itemIndex:0,sectionIndex:sectionIndex})
  }
  _renderHeader(){
    var data = ['大众','本田','别克','丰田']
    var brands = [];
    for(var i = 0; i < data.length ; i ++){
        brands.push(
          <View style={{width:width/3,height:40,alignItems:'center',justifyContent:'center',
            borderColor:'#dedede',borderWidth:1}}
          key={i}>
            <Text style={{fontSize:18}}>{data[i]}</Text>
          </View>
        )
    }
    return(
      <View>
        <View style={styles.headerView}>
            <Text style={styles.headerText}>{'热门品牌'}</Text>
        </View>
        <View style={{flexDirection:'row',flexWrap:'wrap'}}>
            {brands}
        </View>
      </View>
    )
  }
  _getItemLayout(data, index) {
      // console.log(data)
      let [length, separator, header] = [ITEM_HEIGHT, SEPARATOR_HEIGHT, HEADER_HEIGHT];
      let offset = (length + separator) * index + header;
      console.log('index:'+index+'_offset:'+offset);
      return {length, offset:offset , index};
  }

  _renderItem = (item) => {
      return (
          <View style={styles.itemView}>
              <Text style={{marginLeft: 30, fontSize: 16, color: '#333'}}>
                  {item.item.brand}
              </Text>
          </View>
      )
  }

  _renderSectionHeader = (section) => {
    console.log(section)
      return (
          <View style={styles.headerView}>
              <Text style={styles.headerText}>{section.section.key}</Text>
          </View>
      )
  }
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        width: width,
        height: height,
        left: left,
        // top: top,
        backgroundColor: "#000000",
    },
    mask: {
        justifyContent: "center",
        backgroundColor: "#000000",
        opacity: 0.3,
        position: "absolute",
        width: width,
        height: height,
        left: left,
        top: top,
    },
    // 提示标题
    tipTitleView: {
        height: 56,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        marginLeft: 10,
        marginRight: 10
    },
    // 提示文字
    tipTitleText: {
        color: "#999999",
        fontSize: 14,
    },
    // 分割线
    tipContentView: {
        width: width,
        height: 56,
        backgroundColor:'#fff',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
    item:{
        width: width,
        height: 56,
        backgroundColor:'#fff',
        justifyContent: 'center',
        borderRadius: 5,
    },
    button: {
        height: 57,
        backgroundColor: '#fff',
        alignSelf: 'stretch',
        justifyContent: 'center',
        borderRadius: 5,
    },
    // 取消按钮
    buttonText: {
        fontSize: 17,
        color: "#0084ff",
        textAlign: "center",
    },
    content: {
        backgroundColor: '#fff',
        flexDirection:'row',
        borderTopColor:'#dadada',
        borderTopWidth:1,
    },
    region_list:{
      // width:50,
      flex:1,
      backgroundColor:'#ffffff',
      borderRightColor:'#dadada',
      borderRightWidth:1,
    },
    region_item:{
      paddingTop:10,
      paddingBottom:10,
      flexDirection:'row',
    },
    region_item_font:{
      fontSize:20,
      color:'#000000',
      paddingLeft:14,
    },
    slectline:{
      width:2,
      height:20,
      backgroundColor:'#ff6629',
    },
    region_son_list:{
      flex:1,
      backgroundColor:'#f8f9fb',
    },
    headerView: {
        justifyContent: 'center',
        height: HEADER_HEIGHT,
        paddingLeft: 20,
        backgroundColor: '#eee'
    },
    headerText: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#787878'
    },
    itemView: {
        flexDirection: 'row',
        padding: 12,
        alignItems: 'center',
        height: ITEM_HEIGHT
    }
});
