import React,{Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
} from 'react-native';
import { Icon } from 'react-native-elements';
const head = require('../images/head01.jpg')
const CommentData = [
  {
    id:0,
    name:'用户1',
    date:'2017.01.04',
    star:5,
    comment:'味道不错',
  },
  {
    id:1,
    name:'用户1',
    date:'2017.01.03',
    star:4,
    comment:'价格实惠',
  }
]

export default class CommentPage extends Component {
  render(){
    return(
      <View>
      <FlatList
          data={ CommentData }
          renderItem={(item) => this.renderRow(item)}
          ItemSeparatorComponent={ () => this.renderSeparator() }
          keyExtractor={ (item) => item.id }
      />
      </View>
    )
  }
  renderSeparator = () => {
      return (
          <View style={{height:1,backgroundColor:'#e9e9e9'}}/>
      )
  };
  renderRow(item){
    return(
      <View key={item.id} style={{padding:10}}>
      <View style={{flexDirection:'row',}}>
        <Image
          style={{height:30,width:30,resizeMode:'cover',borderRadius:15,}}
           source={head}
           />
        <View style={{flex:1,paddingLeft:10}}>
          <View style={{flexDirection:'row',}}>
              <View style={{flex:1}}>
                <Text style={{fontSize:18}}>{item.item.name}</Text>
                <View style={{marginTop:6,flexDirection:'row'}}>
                  <Text style={{color:'#d3d3d3'}}>评分</Text>
                  {this._renderStar(item.item.star)}
                </View>
              </View>
              <Text style={{color:'#d3d3d3',}}>{item.item.date}</Text>
          </View>
          <Text style={{fontSize:18,marginTop:6,}}>{item.item.comment}</Text>
        </View>
      </View>
    </View>
    )
  }
  _renderStar(star){
    var labels =[];
    for (var i = 0; i < 5; i++) {
      if(i < star){
        labels.push(
        <Icon
        key={i}
        name='star'
        size={20}
        type="MaterialIcons"
        color={'#ffcd60'}/>
        );
      }else{
        labels.push(
        <Icon key={i}
        name='star-border'
        size={20}
        type="MaterialIcons"
        color={'#e5e5e5'}/>
        );
      }
    }
    return(
      <View style={{flexDirection:'row'}}>
        {labels}
      </View>
    )
  }
}
