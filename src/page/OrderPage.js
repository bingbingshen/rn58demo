import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    FlatList,
    SectionList,
    Dimensions,
    TouchableOpacity,
    Image,
} from 'react-native';
import { Icon } from 'react-native-elements';
const img = require('../images/commodity01.jpg')
const ParcelData = [
  {
    section : "热销",
    type:1,
    data : [
      {
        name : "蟹黄汤包",
        sale : "月售875",
        favorite : "赞31",
        money : "20",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "小馄饨",
        sale : "月售875",
        favorite : "赞31",
        money : "10",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "牛杂粉丝汤套餐",
        sale : "月售875",
        favorite : "赞31",
        money : "35",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "鸭血粉丝汤",
        sale : "月售875",
        favorite : "赞31",
        money : "15",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      }
    ]
  },
  {
    section : "折扣",
    type:2,
    data : [
      {
        name : "折扣1",
        sale : "月售875",
        favorite : "赞31",
        money : "20",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "折扣2",
        sale : "月售875",
        favorite : "赞31",
        money : "10",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "折扣3",
        sale : "月售875",
        favorite : "赞31",
        money : "35",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "折扣4",
        sale : "月售875",
        favorite : "赞31",
        money : "15",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      }
    ]
  },
  {
    section : "米饭",
    type:0,
    data : [
      {
        name : "米饭1",
        sale : "月售875",
        favorite : "赞31",
        money : "20",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "米饭2",
        sale : "月售875",
        favorite : "赞31",
        money : "10",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
    ]
  },
  {
    section : "蜀之麻辣香锅",
    type:0,
    data : [
      {
        name : "蜀之麻辣香锅",
        sale : "月售875",
        favorite : "赞31",
        money : "20",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
    ]
  },
  {
    section : "陈记酸辣粉",
    type:0,
    data : [
      {
        name : "陈记酸辣粉1",
        sale : "月售875",
        favorite : "赞31",
        money : "20",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "陈记酸辣粉2",
        sale : "月售875",
        favorite : "赞31",
        money : "10",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
      {
        name : "陈记酸辣粉3",
        sale : "月售875",
        favorite : "赞31",
        money : "35",
        disprice:10,
        discount:5,
        limit:8,
        img:""
      },
    ]
  },
]

var { width, height } = Dimensions.get('window');

let Headers = [];


export default class OrderPage extends Component {

    static navigationOptions = ({ navigation }) => ({
            headerTitle : '联动List',
    });
    constructor(props){
      super(props);
    }
    componentDidMount() {
        ParcelData.map((item, i) => {
            Headers.push(item.section);
        });
    };

    componentWillUnmount() {
        Headers = [];
    };

    renderLRow = (item) => {
        return (
            <TouchableOpacity style={[ styles.lItem, {backgroundColor: item.index == this.state.cell ? 'white' : null} ]}
                              onPress={()=>this.cellAction(item)}>
                {this._renderTag(item.item)}
                <Text style={styles.lText}>{ item.item.section }</Text>
            </TouchableOpacity>
        )
    };
    _renderTag(item){
      if(item.type == 1){
          return(
            <Icon
              name='star-border'
              size={20}
              type="MaterialIcons"
              color={'#ff7761'}
              />
          )
      }else if(item.type == 2){
          return(
            <Icon
              name='local-offer'
              size={20}
              type="MaterialIcons"
              color={'#fbc958'}
              />
          )
      }else{
          return(null)
      }
    }

    cellAction = (item) => {
        if (item.index <= ParcelData.length) {
            this.setState({
                cell : item.index
            });
            // if (item.index > 0) {
            //     var count = 0;
            //     for (var i = 0;
            //         i < item.index;
            //         i++) {
            //         count += ParcelData[ i ].data.length + 1
            //     }
            //     this.refs.MySectionList.scrollToIndex({ animated : false, index : count })
            // } else {
            //     this.refs.MySectionList.scrollToIndex({ animated : false, index : 0 });
            // }
            this.refs.MySectionList.scrollToLocation({ animated:false,itemIndex:0,sectionIndex:item.index })
        }

    };

    itemChange = (info) => {
        let section = info.viewableItems[ 0 ].section.section;
        if (section) {
            let index = Headers.indexOf(section);
            if (index < 0) {
                index = 0;
            }
            this.setState({ cell : index });
        }
    };

    state = {
        cell : 0
    };

    renderRRow = (item) => {
        return (
            <View style={ styles.rItem}>
                <Image style={ styles.icon } source={/*{ uri : item.item.img }*/img}/>
                <View style={ styles.rItemDetail }>
                    <Text style={ styles.foodName }>{ item.item.name }</Text>
                    <View style={ styles.saleFavorite }>
                        <Text style={ [styles.saleFavoriteText,{color:'#919191'} ]}>{ item.item.sale }</Text>
                        <Text style={ [styles.saleFavoriteText,{ marginLeft:15,color:'#919191'}]}>{ item.item.favorite }</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                      <View style={{flex:1,}}>
                        <View style={{flexDirection:'row'}}>
                          <Text style={{fontSize:20,fontWeight:'bold',color:'#ff2841'}}>￥{ item.item.disprice }</Text>
                          <Text style={{fontSize:14,color:'#919191',paddingTop:6,}}>￥{ item.item.money }</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <Icon
                          name='local-offer'
                          size={20}
                          type="MaterialIcons"
                          color={'#ec5a6d'}
                          />
                          <Text style={{color:'#ff2841'}}>{item.item.discount}折 限{item.item.limit}份</Text>
                        </View>
                      </View>
                      <View style={{padding:10}}>
                        <Icon
                          name='control-point'
                          size={20}
                          type="MaterialIcons"
                          color={'#ffd574'}
                          />
                      </View>
                    </View>
                </View>
            </View>
        )
    };

    sectionComp = (section) => {
        return (
            <View style={{flexDirection:'row',height:40,backgroundColor:'#ffffff',/*justifyContent:'center',*/alignItems:'center'}}>
                <View style={{width:2,backgroundColor:'#fbc958',height:20,marginLeft:6,marginRight:6}}></View>
                <Text style={{fontSize:18,color:'#000000',fontWeight:'bold',}}>{section.section.section}</Text>
            </View>
        )
    };

    separator = () => {
        return (
            // <View style={{height:1,backgroundColor:'gray'}}/>
            <View></View>
        )
    };

    render() {
        return (
            <View style={ styles.container }>
                <FlatList
                    ref='MyFlatList'
                    style={ styles.leftList }
                    data={ ParcelData }
                    renderItem={(item) => this.renderLRow(item)}
                    ItemSeparatorComponent={ () => this.separator() }
                    keyExtractor={ (item) => item.section }
                />
                <SectionList
                    ref='MySectionList'
                    style={ styles.rightList }
                    renderSectionHeader={ (section) => this.sectionComp(section) }
                    renderItem={ (item) => this.renderRRow(item) }
                    sections={ ParcelData }
                    keyExtractor={ (item) => item.name }
                    onViewableItemsChanged={ (info) => this.itemChange(info) }
                    // getItemLayout={this._getItemLayout}
                />
                <View style={{flex:1,position: 'absolute',bottom: 0,left: 0,width:width,}}>
                <Icon
                  style={{position: 'absolute',left:5,bottom:3,}}
                  name='shopping-cart'
                  size={60}
                  type="MaterialIcons"
                  color={'#9d9d9b'}
                  />
                  <View style={{backgroundColor:'rgba(0, 0, 0, 0.7)',flex:1,flexDirection:'row',alignItems:'center',justifyContent:'space-around',height:40}}>
                      <Text style={{marginLeft:20,color:'#9d9d9b',fontSize:20}}>免费配送</Text>
                      <Text style={{fontWeight:'bold',color:'#9d9d9b',fontSize:20}}>￥8元起送</Text>
                  </View>
                </View>
            </View>
        );
    }

    _getItemLayout(data, index) {
      // if(index > data.length ){
      //   return 0;
      // }
      // console.log(index)
      // console.log(data[index-1].data)
      // let items = data[index]['data'];
      const ITEM_HEIGHT = 50; //item的高度
      const HEADER_HEIGHT = 24;  //分组头部的高度
      const SEPARATOR_HEIGHT = 0;  //分割线的高度
      let [length, separator, header] = [ITEM_HEIGHT, SEPARATOR_HEIGHT, HEADER_HEIGHT];
      return {length, offset: (length + separator) * index + header, index};
   }
}

const styles = StyleSheet.create({
    container : {
        flexDirection : 'row'
    },
    leftList : {
        width : 1 * width / 4,
        // backgroundColor : '#E9E9EF'
    },
    lItem : {
        minHeight : 44,
        flexDirection : 'row',
        alignItems:'center',
        // justifyContent : 'center',
        padding:10,
    },
    lText : {
        // marginLeft : 10,
        // marginRight : 10,
        fontSize : 16,
        color:'#000000',
    },
    rightList : {
        width : 3 * width / 4
    },
    rItem : {
        flexDirection : 'row',
        backgroundColor:'#ffffff',
        paddingTop:10,
    },
    rItemDetail : {
        flex : 1,
        marginTop : 10,
        marginLeft : 5
    },
    icon : {
        height : 60,
        width : 60,
        marginTop : 10,
        marginBottom : 10,
        marginLeft : 8,
        borderWidth : 1,
        borderColor : '#999999'
    },
    foodName : {
        fontSize : 18,
    },
    saleFavorite : {
        flexDirection : 'row',
        marginTop : 5,
        marginBottom : 5,
    },
    saleFavoriteText : {
        fontSize : 13,
    },
    moneyText : {
        color : 'orange'
    },
});
