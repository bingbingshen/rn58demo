import React from 'react';
import {
  View,
  Text,
  Alert,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
class MainUI extends React.Component {
  render(){
    return(
      <View style={styles.main}>
      <TouchableOpacity style={{borderColor:'#000',borderWidth:1,padding:10}} onPress={()=>this.props.navigation.navigate('SearchUI')}>
        <Text style={{fontSize:30}}>搜索</Text>
      </TouchableOpacity>
      <View style={{height:10,}}></View>
      <TouchableOpacity style={{borderColor:'#000',borderWidth:1,padding:10}} onPress={()=>this.props.navigation.navigate('OrderUI')}>
        <Text style={{fontSize:30}}>点餐</Text>
      </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  }
})
export {MainUI};
