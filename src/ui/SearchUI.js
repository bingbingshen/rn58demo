import React,{Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  NativeModules,
  FlatList,
} from 'react-native';
import { Icon,Button } from 'react-native-elements';
import CityPopup from '../popup/CityPopup';
import JobPopup from '../popup/JobPopup';
import BrandPopup from '../popup/BrandPopup';
const { UIManager } = NativeModules;
const sitems = [
              {id:0,title:'国企_银行渠道专员_高薪',wages:'8000元/月',tag:'其他销售',
              company:'58联合易召直招',company_tag:'自营',pos:'北京',type:1,
              labels:['五险一金','包住','交通补助','无责底薪','无责底薪']},
              {id:1,title:'易点租_销售_高薪急招',wages:'8000-1200元/月',tag:'销售代表',type:2,
              company:'北京梦娜丽莎有限公司',company_tag:'会员',pos:'东城-灯市口',
                labels:['包住','餐补','无责底薪']},
              {id:2,title:'国企_银行渠道专员_高薪',wages:'8000元/月',tag:'其他销售',
              company:'58联合易召直招',company_tag:'自营',pos:'北京',type:1,
              labels:['五险一金','包住','交通补助','无责底薪','无责底薪']},
              {id:3,title:'易点租_销售_高薪急招',wages:'8000-1200元/月',tag:'销售代表',type:2,
              company:'北京梦娜丽莎有限公司',company_tag:'会员',pos:'东城-灯市口',
                labels:['包住','餐补','无责底薪']}
            ];
class SListItem extends React.PureComponent {
  _onPress = () => {
    this.props.onPressItem(this.props.id);
  };

  render() {
    return (
      <TouchableOpacity
        {...this.props}
        onPress={this._onPress}
      >
      <View style={styles.item_bg}>
        <View style={styles.item}>
          <View style={styles.item0}>
              <View style={styles.item_left}>
                <Text style={styles.item_title}>
                  {this.props.item.title}
                </Text>
                <View style={styles.item_wages_bg}>
                  <Text style={styles.item_wages}>{this.props.item.wages}</Text>
                  <View style={{width:20}}></View>
                  <Icon
                    name='star-border'
                    size={20}
                    type="MaterialIcons"
                    color={'#b6b6b6'}
                    />
                  <Text style={styles.item_tag}>{this.props.item.tag}</Text>
                </View>
                {this._renderLabel(this.props.item)}
              </View>
              <View style={styles.item_right}>
                {this._renderType(this.props.item)}
                <View style={styles.item_rinterval}></View>
                <View style={styles.item_see}><Text style={styles.item_see_font}>查看</Text></View>
              </View>
          </View>
          <View style={{borderWidth:1,borderColor:'#d0d0d0',borderStyle : 'dashed'}}></View>
          <View style={styles.item1}>
            <View style={styles.item_company}>
              <View style={styles.item_company_tag}><Text style={styles.item_company_tag_font}>{this.props.item.company_tag}</Text></View>
              <Text style={styles.item_company_font}>{this.props.item.company}</Text>
            </View>
            <View style={styles.item_pos}><Text style={styles.item_pos_font}>{this.props.item.pos}</Text></View>
          </View>
        </View>
      </View>
      </TouchableOpacity>
    )
  }
  _renderType(item){
    if(item.type == 2){
      return(<View style={styles.item_hire}><Text style={{color:'#c7c7c7',}}>精准</Text></View>);
    }else{
      return(
        <View style={styles.item_hire}>
          <View style={styles.item_lhire}><Text style={styles.item_lhire_font}>速聘</Text></View>
          <View style={styles.item_rhire}><Text style={styles.item_rhire_font}>58自营</Text></View>
        </View>
      )
    }
  }
  _renderLabel(itme){
    var labels =[];
    for (var i = 0; i < itme.labels.length; i++) {
      if(i > 3){
        break;
      }
      labels.push(
      <View key={i} style={styles.item_label}><Text style={styles.item_label_font}>{itme.labels[i]}</Text></View>
      );
    }
    if(itme.labels.length > 4){
      labels.push(
        <Text key={4} style={styles.item_label_font}>{'...'}</Text>
      )
    }
    return(
      <View style={styles.item_label_bg}>
        {labels}
      </View>
    )
  }
}
export default class SearchUI extends Component{
  constructor(props){
    super(props);
    this.state = {
      showcity:false,
      city_color:'#000000',
      city_icon_color:'#c9c9c9',
      city_icon_name:'expand-more',
      showjob:false,
      job_color:'#000000',
      job_icon_color:'#c9c9c9',
      job_icon_name:'expand-more',
      selected: (new Map(): Map<string, boolean>),
      sitems:sitems,
      showbrand:false,
      brand_color:'#000000',
      brand_icon_color:'#c9c9c9',
      brand_icon_name:'expand-more',
    };
    this.callbackCityPopup = this.callbackCityPopup.bind(this);
    this.callbackJobPopup = this.callbackJobPopup.bind(this);
    this.callbackBrandPopup = this.callbackBrandPopup.bind(this);
  }
  render(){
    return(
      <View style={styles.main}>
        {this._renderNav()}
        {this._renderMenu()}
        <FlatList
          data={this.state.sitems}
          extraData={this.state.selected}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          />
        <CityPopup ref={(citypopup)=>{
                   this.citypopup = citypopup;
               }} />
       <JobPopup ref={(jobpopup)=>{
                  this.jobpopup = jobpopup;
              }} />
        <BrandPopup  ref={(brandpopup)=>{
                   this.brandpopup = brandpopup;
               }} />
      </View>
    )
  }
  _renderNav(){
    return(
      <View style={styles.nav}>
          <TouchableOpacity style={styles.nav_left}
            onPress={()=>this.props.navigation.goBack()}>
            <Icon
              name='chevron-left'
              size={40}
              type="MaterialIcons"
              color={'#626262'}
            />
          </TouchableOpacity>
          <View style={styles.nav_search}>
              <View style={styles.search}>
                <Icon
                  name='search'
                  size={30}
                  type="MaterialIcons"
                  color={'#a9a9a9'}
                />
                <Text style={styles.search_font}>
                  搜索销售
                </Text>
              </View>
          </View>
          <View style={styles.nav_right}>
           <Icon
            name='filter-none'
            size={24}
            type="MaterialIcons"
            color={'#626262'}
          />
          </View>
      </View>
    )
  }
  _renderMenu(){
    return(
      <View style={styles.menu} ref="menu">
        <TouchableOpacity style={styles.menu_item}
        onPress={()=>{
          if(this.state.showjob){
            this.hideJobPopup();
          }
          if(this.state.showbrand){
            this.hideBrandPopup();
          }
          if(!this.state.showcity){
            this.refs.menu.measure((x,y,width,height,pageX,pageY)=>{
                console.log(x,y,width,height,pageX,pageY,);
                this.showCityPopup(pageY+height);
            });
          }else{
              this.hideCityPopup();
          }
          var showcity = !this.state.showcity;
          this.refreshCity(showcity);
        }}>
          <Text style={[styles.menu_item_font,{color:this.state.city_color}]}>全北京</Text>
          <Icon
           name={this.state.city_icon_name}
           size={24}
           type="MaterialIcons"
           color={this.state.city_icon_color}
           />
        </TouchableOpacity >
        <View style={styles.menu_pline}></View>
        <TouchableOpacity style={styles.menu_item}
          onPress={()=>{
            console.log(this.state)
            if(this.state.showcity){
              this.hideCityPopup();
            }
            if(this.state.showbrand){
              this.hideBrandPopup();
            }
            if(!this.state.showjob){
              this.refs.menu.measure((x,y,width,height,pageX,pageY)=>{
                  console.log(x,y,width,height,pageX,pageY,);
                  this.showJobPopup(pageY+height);
              });
            }else{
                this.hideJobPopup();
            }
            var showjob = !this.state.showjob;
            this.refreshJob(showjob);
          }}>
          <Text style={[styles.menu_item_font,{color:this.state.job_color}]}>职位</Text>
          <Icon
           name={this.state.job_icon_name}
           size={24}
           type="MaterialIcons"
           color={this.state.job_icon_color}
           />
        </TouchableOpacity >
        <View style={styles.menu_pline}></View>
        {/* <View style={styles.menu_item}>
          <Text style={styles.menu_item_font}>薪资</Text>
          <Icon
           name='expand-more'
           size={24}
           type="MaterialIcons"
           color={'#c9c9c9'}
           />
        </View> */}
        <View style={styles.menu_pline}></View>
        <TouchableOpacity style={styles.menu_item}
        onPress={()=>{
          if(this.state.showcity){
            this.hideCityPopup();
          }
          if(this.state.showjob){
            this.hideJobPopup();
          }
          if(!this.state.showbrand){
            this.refs.menu.measure((x,y,width,height,pageX,pageY)=>{
                console.log(x,y,width,height,pageX,pageY,);
                this.showBrandPopup(pageY+height);
            });
          }else{
              this.hideBrandPopup();
          }
          var showbrand = !this.state.showbrand;
          this.refreshBrand(showbrand);
        }}>
          <Text style={[styles.menu_item_font,{color:this.state.brand_color}]}>品牌</Text>
          <Icon
           name={this.state.brand_icon_name}
           size={24}
           type="MaterialIcons"
           color={this.state.brand_icon_color}
           />
        </TouchableOpacity>
      </View>
    )
  }
  _keyExtractor = (item, index) => item.id;
  _onPressItem = (id: string) => {
    // updater functions are preferred for transactional updates
    this.setState((state) => {
      // copy the map rather than modifying state.
      const selected = new Map(state.selected);
      selected.set(id, !selected.get(id)); // toggle
      return {selected};
    });
  };
  _renderItemSeparatorComponent = ({highlighted}) => (
       <View style={{ height:1, backgroundColor:'#e0e0e0' }}></View>
   );
  _renderItem = ({item}) => (
    <SListItem
      id={item.id}
      onPressItem={this._onPressItem}
      selected={!!this.state.selected.get(item.id)}
      item={item}
    />
  );
  showCityPopup(y){
      this.citypopup.show(y,this.callbackCityPopup);

  }
  hideCityPopup(){
      this.citypopup.cancel();
  }
  // 回调
  callbackCityPopup(hide){
      // console.log(hide);
      this.refreshCity(!hide);
  }
  showJobPopup(y){
      this.jobpopup.show(y,this.callbackJobPopup);

  }
  hideJobPopup(){
      this.jobpopup.cancel();
  }
  showBrandPopup(y){
      this.brandpopup.show(y,this.callbackBrandPopup);

  }
  hideBrandPopup(){
      this.brandpopup.cancel();
  }
  // 回调
  callbackCityPopup(hide){
      this.refreshCity(!hide);
  }
  callbackJobPopup(hide){
    this.refreshJob(!hide);
  }
  callbackBrandPopup(hide){
    this.refreshBrand(!hide);
  }
  getMenuColor(isSelect){
    if(isSelect){
      return '#ff751b';
    }else{
      return '#000000';
    }
  }
  getMenuIconColor(isSelect){
    if(isSelect){
      return '#ff751b';
    }else{
      return '#c9c9c9';
    }
  }
  getMenuIconName(isSelect){
    return isSelect?'expand-less':'expand-more';
  }
  refreshCity(show){
    var color = this.getMenuColor(show);
    var iconcolor = this.getMenuIconColor(show);
    var iconname = this.getMenuIconName(show);
    this.setState({
      showcity:show,
      city_color:color,
      city_icon_color:iconcolor,
      city_icon_name:iconname,
    })
  }
  refreshJob(show){
    var color = this.getMenuColor(show);
    var iconcolor = this.getMenuIconColor(show);
    var iconname = this.getMenuIconName(show);
    this.setState({
      showjob:show,
      job_color:color,
      job_icon_color:iconcolor,
      job_icon_name:iconname,
    })
  }
  refreshBrand(show){
    var color = this.getMenuColor(show);
    var iconcolor = this.getMenuIconColor(show);
    var iconname = this.getMenuIconName(show);
    this.setState({
      showbrand:show,
      brand_color:color,
      brand_icon_color:iconcolor,
      brand_icon_name:iconname,
    })
  }
}
const styles = StyleSheet.create({
  main:{
    flex:1,
    backgroundColor:'#f6f6f6',
  },
  nav:{
    paddingTop:20,
    flexDirection:'row',
  },
  nav_left:{
    width:40,
    justifyContent:'center',
    alignItems:'center',
  },
  nav_right:{
    width:40,
    justifyContent:'center',
    alignItems:'center',
  },
  nav_search:{
    flex:1,
    padding:10,
  },
  search:{
    backgroundColor:'#ffffff',
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
  },
  search_font:{
    color:'#a9a9a9',
    fontSize:20,
  },
  menu:{
      flexDirection:'row',
      backgroundColor:'#ffffff',
      paddingTop:10,
      paddingBottom:10,
  },
  menu_item:{
    flex:1,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
  },
  menu_pline:{
    width:2,
    height:30,
    backgroundColor:'#e7e7e7',
  },
  menu_item_font:{
      fontSize:18,
      color:'#000000',
      fontWeight:'bold',
  },
  item_bg:{
    paddingTop:10,
    paddingBottom:10,
  },
  item:{
    backgroundColor:'#fefefe',
    padding:10,
  },
  item0:{
    flexDirection:'row',
    paddingTop:10,
    paddingBottom:10,
  },
  item1:{
    flexDirection:'row',
    paddingTop:10,
    paddingBottom:10,
  },
  item_left:{
      flex:1,
  },
  item_right:{
    width:80,
    alignItems:'flex-end',
  },
  item_title:{
    fontSize:24,
    color:'#000000',
    fontWeight:'bold',
  },
  item_see:{
    width:60,
    backgroundColor:'#fb663b',
    borderRadius:5,
    padding:10
  },
  item_see_font:{
    fontSize:16,
    color:'#ffffff',
  },
  item_hire:{
    flexDirection:'row',
  },
  item_lhire:{
    backgroundColor:'#49a6f3',
    borderBottomLeftRadius:5,
    borderTopLeftRadius:5,
    padding:2,
  },
  item_rhire:{
    backgroundColor:'#ffffff',
    borderTopWidth:1,
    borderTopColor:'#49a6f3',
    borderBottomWidth:1,
    borderBottomColor:'#49a6f3',
    borderRightWidth:1,
    borderRightColor:'#49a6f3',
    borderTopRightRadius:5,
    borderBottomRightRadius:5,
    padding:2,
  },
  item_lhire_font:{
    fontSize:14,
    color:'#ffffff',
  },
  item_rhire_font:{
    fontSize:14,
    color:'#49a6f3',
  },
  item_rinterval:{
    height:10,
  },
  item_wages_bg:{
    flex:1,
    flexDirection:'row',
    // justifyContent:'center',
    alignItems:'center',
    paddingTop:10,
    paddingBottom:10,
  },
  item_wages:{
    color:'#fb663b',
    fontSize:20,
    fontWeight:'bold',
  },
  item_tag:{
    color:'#7f7f7f',
  },
  item_label_bg:{
    flex:1,
    // height:40,
    flexDirection:'row',
    // backgroundColor:'#ff00ff',
  },
  item_label:{
    marginLeft:4,
    // position:'absolute',
    borderTopWidth:1,
    borderTopColor:'#49a6f3',
    borderBottomWidth:1,
    borderBottomColor:'#49a6f3',
    borderLeftWidth:1,
    borderLeftColor:'#49a6f3',
    borderRightWidth:1,
    borderRightColor:'#49a6f3',
    borderTopLeftRadius:5,
    borderBottomLeftRadius:5,
    borderTopRightRadius:5,
    borderBottomRightRadius:5,
    padding:5,
  },
  item_label_font:{
    color:'#49a6f3',
    fontSize:12,
  },
  item_company:{
    flex:1,
    flexDirection:'row',
    alignItems:'center',
  },
  item_company_tag:{
      backgroundColor:'#49a6f3',
      borderBottomRightRadius:10,
      borderTopRightRadius:10,
      width:40,
      padding:4,
  },
  item_company_tag_font:{
      color:'#ffffff',
  },
  item_company_font:{
    fontSize:20,
    color:'#878787',
  },
  item_pos:{
    width:100,
    alignItems:'flex-end',
  },
  item_pos_font:{
    fontSize:16,
    color:'#878787',
  }
});
