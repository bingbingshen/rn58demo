import React,{Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  NativeModules,
  FlatList,
  Image,
} from 'react-native';
import { Icon,Button } from 'react-native-elements';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import OrderPage from '../page/OrderPage';
import CommentPage from '../page/CommentPage';
import SellerPage from '../page/SellerPage';
import OrderTabBar from '../custom/OrderTabBar';
const logo = require('../images/commodity01.jpg')
const shops = {name:'焖锅鸡米饭',
  tips:'本店最新推出酸辣粉真的是特别好吃，欢迎关注！',
  give:'满50元赠红苹果一瓶'}
export default class OrderUI extends Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <View style={styles.main}>
      <View style={styles.header}>
      {this._renderNav()}
      {this._renderShops()}
      </View>
      <ScrollableTabView
        renderTabBar={() => <OrderTabBar />}>
        <OrderPage tabLabel='点菜'/>
        <CommentPage tabLabel='评价'/>
        <SellerPage tabLabel='商家'/>
       </ScrollableTabView>
      </View>
    )
  }
  _renderNav(){
    return(
      <View style={styles.nav}>
          <TouchableOpacity style={styles.nav_left}
            onPress={()=>this.props.navigation.goBack()}>
            <Icon
              name='chevron-left'
              size={40}
              type="MaterialIcons"
              color={'#ffffff'}
            />
          </TouchableOpacity>
          <View style={styles.nav_right}>
             <Icon
              name='search'
              size={24}
              type="MaterialIcons"
              color={'#ffffff'}
            />
            <View style={{width:10}}></View>
            <Icon
             name='exit-to-app'
             size={24}
             type="MaterialIcons"
             color={'#ffffff'}
           />
           <View style={{width:10}}></View>
           <View style={styles.pindan}>
              <Text style={styles.pindan_font}>{'拼单'}</Text>
           </View>
          </View>
      </View>
    )
  }
  _renderShops(){
    return(
      <View style={{padding:10}}>
      <View style={{flexDirection:'row',alignItems:'center'}}>
      <View style={{flex:1,flexDirection:'row'}}>
        <Image
          style={styles.logo}
          source={logo}
        />
        <View style={{padding:10}}>
          <Text style={{fontSize:22,color:'#ffffff'}}>{shops.name}</Text>
          <View style={{flex:1,flexDirection:'row',alignItems:'center',paddingTop:10,}}>
            <Icon
              name='volume-mute'
              size={24}
              type="MaterialIcons"
              color={'#ffffff'}
            />
            <Text style={{fontSize:14,color:'#ffffff',width:180,}}
            numberOfLines={1}>{shops.tips}</Text>
          </View>
        </View>
      </View>
      <View style={{width:1,backgroundColor:'#ffffff',height:50}}></View>
      <View style={{marginLeft:10,}}>
        <Icon
          name='star'
          size={30}
          type="MaterialIcons"
          color={'#fdd261'}
        />
        <Text style={{fontSize:16,color:'#ffffff'}}>{'已收藏'}</Text>
      </View>
      </View>
      <View style={{height:10}}></View>
      <View style={{borderWidth:1,borderColor:'#d0d0d0',borderStyle : 'dashed'}}></View>
      <View style={{flexDirection:'row',justifyContent:'space-around',paddingTop:10,}}>
        <View style={{flex:1,flexDirection:'row',}}>
        <Text style={{backgroundColor:'#fdd261',fontSize:18,color:'#ffffff',width:30,padding:4,}}>{'赠'}</Text>
        <View style={{width:4}}></View>
        <Text style={{color:'#ffffff',fontSize:18,}}>{shops.give}</Text>
        </View>
        <Text style={{color:'#ffffff',fontSize:18,}}>{'5个活动>'}</Text>
      </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main:{
    flex:1,
    backgroundColor:'#f6f6f6',
  },
  header:{
    backgroundColor:'#c8984c',
  },
  nav:{
    paddingTop:20,
    flexDirection:'row',
  },
  nav_left:{
    width:40,
    justifyContent:'center',
    alignItems:'center',
  },
  nav_right:{
    flex:1,
    flexDirection:'row',
    justifyContent:'flex-end',
    alignItems:'center',
    paddingRight:10,
  },
  pindan:{
    borderWidth:1,
    borderColor:'#ffffff',
    borderRadius:2,
  },
  pindan_font:{
    color:'#ffffff',
    fontSize:14,
  },
  logo:{
     height:60,
     width:60,
     // 设置图片填充模式
    resizeMode:'cover',
    // 设置圆角
    borderRadius:30,
  },
})
