#页面Demo#
![](https://bytebucket.org/bingbingshen/rn58demo/raw/3fa13fc24f90a7ed7955f4b8c0cc4e1cbce17839/gif/搜索页.gif)
![](https://bytebucket.org/bingbingshen/rn58demo/raw/3fa13fc24f90a7ed7955f4b8c0cc4e1cbce17839/gif/点餐页.gif)
## 已知问题
* 点击搜索页中的下拉菜单弹出动画屏幕闪动问题
* 点击搜索页中的下拉菜单品牌选择中的索引选择列表位置问题
* 点餐页中的联动列表偏移位置偏差问题

## 参考链接
* [react-native 封装选择弹出框(ios&android)](http://www.jianshu.com/p/42b4390e860e)

* [React Native使用SectionList打造城市选择列表，包含分组的跳转](http://blog.csdn.net/sinat_17775997/article/details/71424324)

* [22-React-Native-左右联动List](http://www.jianshu.com/p/bf9d20e3986e)


